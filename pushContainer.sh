#!/bin/bash

if [[ "$DOCKER_PASS" == "" ]]; then
  echo "The DOCKER_PASS variable is not set."
  exit 1
fi

docker login -u=bella25 -p="$DOCKER_PASS"
docker push bella25/geometaparser:version$(cat VERSION)
