#!/bin/bash


read -p "Please enter the GEO ID: " geoID
read -p "Please enter the file path from which to read the clinical data: " clinicalFilePath
while [ ! -f $clinicalFilePath ]
do
  read -p "File does not exist. Please enter the file path from which to read the clinical data: " clinicalFilePath
done

specFilePath3=Clinical_Specs/${geoID}_Clinical_Specs_Vars.txt

mkdir -p Clinical_Specs


toEdit="NA"
if [ -f $specFilePath3 ]; then
   read -p "A variable specification file already exists for this GEO ID. Would you like to edit it? y/n: " edit
   toEdit=$edit
fi
#echo $toEdit
if [ $toEdit != "n" ]; then
   python3 chooseVars.py $clinicalFilePath $specFilePath3
fi
 
rm -f -r ${geoID}/C*
Rscript --vanilla generalScript.R $geoID $clinicalFilePath $specFilePath3
