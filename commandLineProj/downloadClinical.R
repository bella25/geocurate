source("newCommonForProject.R")
library(stringr)
geoID <- commandArgs()[7]
outputRawFilePath <- commandArgs()[8]

#print(geoID)
dataSetIndex = 1
suppressPackageStartupMessages(library(GEOquery))

if (!file.exists(outputRawFilePath)) {
  #Download data
  if (grepl("_", geoID)) {
    parts <- str_split(geoID, "_")[[1]]
    geoID <- parts[1]
    subname <- parts[2]
    if (subname == "U133A") {
      dataSetIndex = 2
    }
  }
  expressionSet <- getGEO(geoID, GSEMatrix =TRUE, getGPL=FALSE, destdir = "Clinical_Temp")

  if (dataSetIndex > length(expressionSet))
    stop(paste("The dataSetIndex value was ", dataSetIndex, " but there are only ", length(expressionSet), " objects in GEO.", sep=""))
  
  # Retrieve data values depending on the platform
  #if (length(expressionSet) > 1) idx <- grep("GPL6947", attr(expressionSet, "names")) else idx <- 1
  #expressionSet <- expressionSet[[idx]]
  expressionSet <- expressionSet[[dataSetIndex]]
  
  # Extract meta data frame
  metaData <- pData(expressionSet)
  if (!dir.exists(dirname(outputRawFilePath)))
    dir.create(dirname(outputRawFilePath), recursive=TRUE, showWarnings=FALSE)
  
  metaData <- filterUninformativeCols(metaData)
  metaData <- cbind(SampleID = rownames(metaData), metaData)
  write.table(metaData, outputRawFilePath, sep = "\t", row.names = FALSE, col.names = TRUE, quote = FALSE)
}
