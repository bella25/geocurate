#!/bin/bash


read -p "Please enter the GEO ID: " geoID

rawFilePath=Clinical_Raw/${geoID}_Clinical_Raw.txt
rawFilePathTwo=Clinical_Raw/${geoID}_Clinical_Raw_Extract.txt
specFilePath=Clinical_Specs/${geoID}_Clinical_Specs.txt
clinicalFilePathOne=${geoID}/${geoID}_ClinicalOne.txt
clinicalFilePath=${geoID}/${geoID}_Clinical.txt
specFilePath2=Clinical_Specs/${geoID}_Clinical_Specs_Groups.txt
specFilePath3=Clinical_Specs/${geoID}_Clinical_Specs_Vars.txt

mkdir -p Clinical_Temp
mkdir -p Clinical_Raw
mkdir -p Clinical_Specs

toEdit="NA"
if [ -f $specFilePath ]; then
   read -p "A specification file already exists for this GEO ID. Would you like to edit it? y/n: " edit
   toEdit=$edit
fi
#echo $toEdit
if [ $toEdit != "n" ]; then
   Rscript --vanilla downloadClinical.R $geoID $rawFilePath
#   python3 checkMergedData.py $rawFilePath
#   isMerged=$?
#   if [ $isMerged == 1 ]; then
#      echo "Unmerge please"
      #ask about unmerging the thing
      #if yes, have a python script take care of it for you
#   fi 
   python3 interactive.py $rawFilePath $rawFilePathTwo $specFilePath
  #rm -f $rawFilePathTwo
fi

read -p "Please specify the destination file path for this clinical file: " userFilePath
clinicalFilePath=$userFilePath
Rscript --vanilla getClinicalData.R $geoID $rawFilePathTwo $clinicalFilePath $specFilePath

#toEdit="NA"
#if [ -f $specFilePath2 ]; then
#   read -p "A groups specification file already exists for this GEO ID. Would you like to edit it? y/n: " edit
#   toEdit=$edit
#fi
#echo $toEdit
#if [ $toEdit != "n" ]; then
#   python3 interactivePart2.py $clinicalFilePathOne $specFilePath2
#   read -p "Please specify the destination file path for this clinical file: " userFilePath
#   clinicalFilePath=$userFilePath
#   Rscript --vanilla finalize.R $geoID $clinicalFilePathOne $clinicalFilePath $specFilePath2

#fi


toMLI="NA"
read -p "Would you like to specify variables for machine learning? y/n: " toMLI
if [ $toMLI != "n" ]; then
  toEdit="NA"
  if [ -f $specFilePath3 ]; then
     read -p "A variable specification file already exists for this GEO ID. Would you like to edit it? y/n: " edit
     toEdit=$edit
  fi
#echo $toEdit
  if [ $toEdit != "n" ]; then
     python3 chooseVars.py $clinicalFilePath $specFilePath3
  fi
 
  rm -f -r ${geoID}/C*
  Rscript --vanilla generalScript.R $geoID $clinicalFilePath $specFilePath3
fi
