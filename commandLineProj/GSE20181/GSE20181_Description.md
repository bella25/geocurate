## Title
Letrozole (Femara) early and late responses to treatment

## Organism
Homo sapiens

## Experiment type
Expression profiling by array

## Summary
In the present investigation, we have exploited the opportunity provided by neoadjuvant treatment of a group of postmenopausal women with large operable or locally advanced breast cancer (in which therapy is given with the primary tumour remaining within the breast) to take sequential biopsies of the same cancers before and after 10-14 days or 90 days treatment with letrozole.  RNA extracted from the biopsies has been subjected to Affymetrix microarray analysis and the data from paired biopsies interrogated to discover genes whose expression is most influenced by oestrogen deprivation. Keywords: Timecourse between subjects

## Citation
[PubMed article](https://www.ncbi.nlm.nih.gov/pubmed/20697427 20646288)

