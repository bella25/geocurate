from sys import argv
import pandas

LINE = '*'.center(79, '*') + '\n'


class Variable():
  """Parent class for class and covariate variables."""
  def __init__(self, colName, varName, varPrefix, dataType, impute, dummify, standardize):
    self.colName = colName
    self.varName = varName
    self.varPrefix = varPrefix 
    self.dataType = dataType
    self.impute = impute
    self.dummify = dummify
    self.standardize = standardize

#gets user input, and asks again if the user messes up 
def errorChecker(prompt, desiredVals = ["y", "n"]):
  userInput = input(prompt)

  if userInput != "":
    if desiredVals == "integer":
      while type(userInput) == str:
        try:
          userInput = int(userInput)
        except ValueError:
          print("Try again. Please enter an integer.")
          userInput = input(prompt)

    elif desiredVals != "none":
      while userInput not in desiredVals:
        print("Try again. Please enter " + str(desiredVals))
        userInput = input(prompt)
  print("")    

  return userInput

#prints a head of the column and some stats about range and if the values are all different
def printVarsSummary(dataframe, varName):
  if dataframe[varName].dtype == "int64" or dataframe[varName].dtype == "float64":
    print(pandas.DataFrame(dataframe[varName].head(5)))
    print("")
    print("Values range from " + str(min(dataframe[varName])) + " to " + str(max(dataframe[varName])) + ".\n")
  else:
    uniqueVals = pandas.unique(dataframe[varName])
    print('The variable "' + str(varName) + '" has these unique values:')
    for value in uniqueVals:
      print('\t' + str(value))
    print("")

#generalized function for choosing new groups within a single variable
def decideNewGroups(dataframe, column):
  mergedGroups = {}
  shouldBeMerged = errorChecker("Would you like to merge some of the groups for this variable? y/n: ")
  if shouldBeMerged == "y":
    newGroups = []
    newGroup = errorChecker('Please enter the names of the new groups you would like to make or ENTER when finished: ', desiredVals = "none")
    while newGroup != "":
      newGroups.append(newGroup)
      newGroup = errorChecker('Please enter the names of the new groups you would like to make or ENTER when finished: ', desiredVals = "none")
    oldGroups = list(pandas.unique(dataframe[column]))
    oldGroups.append("") 
    for group in newGroups:
      groupToAdd = errorChecker('Please enter the variables you would like to add to "' + group + '" or ENTER when finished: ', desiredVals = oldGroups)
      mergedGroups[group] = []
      while groupToAdd != "":
        oldGroups.pop(oldGroups.columns.get_loc(groupToAdd))
        mergedGroups[group].append(groupToAdd)
        groupToAdd = errorChecker('Please enter the variables you would like to add to "' + group + '" or ENTER when finished: ', desiredVals = oldGroups)
  return mergedGroups

#used specifically for survival/relapse variables
def discretizeSurvival(clinicalData):
  colNames = clinicalData.columns.values.tolist()
  print("Here are the column names of the data:")
  for colName in colNames:
    print("\t" + colName)
  print("")
  relapseDesiredVals = colNames
  relapseDesiredVals.append("")
  relapseName = errorChecker("Please specify the name of the outcome column (or ENTER when finished): ", desiredVals = relapseDesiredVals) 
  survivalDict = {}
  preferredNameDict = {}
  while relapseName != "":
    survivalName = errorChecker("Please specify the name of the time column: ", desiredVals = colNames)
    cutoff = errorChecker("'LongSurvival' describes patients with a time value greater than (enter an integer): ", desiredVals = "integer")
    survivalDict[relapseName] = {"Relapse": relapseName, "Survival": survivalName, "Cutoff": cutoff}
    preferredNameDict[relapseName] = errorChecker('What would you like this new discretized variable to be called (e.g. "Relapse_Group")? ', desiredVals = "none")
    relapseName = errorChecker("Please specify the name of the outcome column (or ENTER when finished): ", desiredVals = relapseDesiredVals) 
  return preferredNameDict, survivalDict
 
#general discretizing function that calls the different types of discretizing functions
def discretizeGroups(dataframe, varsToKeep):
  print("1: Manually select new groups for a single variable.")
  print("2: Discretize a survival column.")
  print("3: Discretize ordinal or numeric groups.")

  option = errorChecker("Please choose an option: ", desiredVals = ["1", "2", "3"])

  if option == "1":
    option_1_DesiredVals = list(dataframe)
    option_1_DesiredVals.append("")
    varToDiscretize = errorChecker("For which variable would you like to change the groups? (or ENTER when finished): ", desiredVals = option_1_DesiredVals)
    while varToDiscretize != "":
      varsToKeep[dataframe.columns.get_loc(varToDiscretize)].newGroups = decideNewGroups(dataframe, varToDiscretize)
      varToDiscretize = errorChecker("For which variable would you like to change the groups? (or ENTER when finished): ", desiredVals = option_1_DesiredVals)

  if option == "2":
    preferredName, discreteSurvival = discretizeSurvival(dataframe)
    for relapseCol in discreteSurvival:
      variable = varsToKeep[dataframe.columns.get_loc(relapseCol)]
      variable.newGroups = "surv" + str(discreteSurvival[relapseCol])
      variable.preferredName = preferredName[relapseCol]

  if option == "3":
    print("Discretize ordinal or numeric groups.")
  return varsToKeep

def substituteVals(dataframe, varsToKeep):

  colNames = dataframe.columns.values.tolist()
  print("Here are the column names of the data:")
  for colName in colNames:
    print("\t" + colName)
  print("")
  subDesiredVals = colNames
  subDesiredVals.append("")


  targetCol = errorChecker("Please specify which column you would like to substitute values for (or ENTER when finished): ", desiredVals = subDesiredVals)
  while targetCol != "":

    uniqueVals = pandas.unique(dataframe[targetCol])
    if len(uniqueVals) > 10:
      print('Here are the first 10 unique values of "' + str(targetCol) + '":')
      for i in range(10):
        print('\t' + str(uniqueVals[i]))
    else:
      print('The variable "' + str(targetCol) + '" has these unique values:')
      for value in uniqueVals:
        print('\t' + str(value))
    print("")


    valsToSub = {}
    toSub = errorChecker("Which old value would you like to replace? (or ENTER when finished) ", desiredVals = "none")
    while toSub != "":
      newVal = errorChecker("Which new value would you like to replace it with? ", desiredVals = "none")
      valsToSub[toSub] = newVal
      toSub = errorChecker("Which old value would you like to replace? (or ENTER when finished) ", desiredVals = "none")
    varsToKeep[dataframe.columns.get_loc(targetCol)].substitutions = valsToSub
    targetCol = errorChecker("Please specify which column you would like to substitute values for (or ENTER when finished): ", desiredVals = subDesiredVals)

  return varsToKeep


#download file
filePath = argv[1] 
oFilePath = argv[2]

dataframe = pandas.read_table(filePath, index_col = 0)

classVars = []
for column in dataframe:

  print(LINE)

  printVarsSummary(dataframe = dataframe, varName = column)

  isClassVar = errorChecker('Would you like to use "' + str(column) + '" as a class variable? y/n: ')
  if isClassVar == "y":
    print('"' + str(column) + '" will be used as a class variable.')

    changeName = errorChecker("Would you like to specify a different name for this variable? y/n: ")
    if changeName == "y":
      varName = errorChecker("What should this new variable be called? ", desiredVals = "none")
    else:
      varName = column

    classPrefix = errorChecker('What type of class variable is this? (e.g. "Histological") ', desiredVals = "none")

    datatype = "factor"

    if dataframe[column].dtype == "int64" or dataframe[column].dtype == "float64":
      dataType = errorChecker("Is this a factor or numeric type variable? ", desiredVals = ["factor", "numeric"])

    classVar = Variable(colName = column, varName = varName, varPrefix = classPrefix, dataType = dataType, impute = "n", dummify = "n", standardize = "n")
    classVars.append(classVar)
  print(LINE)

#choose the covariates to use for each classVar
allVars = {}
for classVar in classVars:
  covariates = []
  for column in dataframe:  
    printVarsSummary(dataframe = dataframe, varName = column)
    isCovariate = errorChecker('Would you like to use this variable as a covariate for"' + str(classVar.colName) + '"? y/n: ')

    print(LINE)    

    if isCovariate == "y":

      changeName = errorChecker("Would you like to specify a different name for this variable? y/n: ")
      if changeName == "y":
        varName = errorChecker("What should this new variable be called? ", desiredVals = "none")
      else:
        varName = column

      covariatePrefix = errorChecker('What type of covariate variable is this? (e.g. "Histological") ', desiredVals = "none")
      toImpute = errorChecker("Would you like to impute missing values for this variable? y/n: ")

      datatype = "factor"

      if dataframe[column].dtype == "int64" or dataframe[column].dtype == "float64":
        dataType = errorChecker("Is this a factor or numeric type variable? ", desiredVals = ["factor", "numeric"])

      toDummify = "n"
      toStandardize = "n"
      if dataType == "factor":
        toDummify = errorChecker("Would you like to dummify this discrete variable? y/n: ")
      if dataType == "numeric":
        toStandardize = errorChecker("Would you like to standardize this continuous variable? y/n: ")

      var = Variable(colName = column, varName = varName, varPrefix = covariatePrefix, dataType = dataType, impute = toImpute, dummify = toDummify, standardize = toStandardize)
 
      covariates.append(var)
    print(LINE)

  allVars[classVar] = covariates

print("That was all the variables.")

with open(oFilePath, 'w+') as myFile:
  myFile.write("Type\tName\tPreferredName\tPrefix\tDataType\tImpute\tDummify\tStandardize\n")
  for classVar, covariates in allVars.items():
    myFile.write("Class\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n".format(classVar.colName, classVar.varName, classVar.varPrefix, classVar.dataType, classVar.impute, classVar.dummify, classVar.standardize))
    for covariate in covariates:
      myFile.write("Covariate\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n".format(covariate.colName, covariate.varName, covariate.varPrefix, covariate.dataType, covariate.impute, covariate.dummify, covariate.standardize))



#download file
filePath = argv[1] 
oFilePath = argv[2]
dataframe = pandas.read_table(filePath, index_col = 0)

varsToKeep = []
for column in dataframe:
  varsToKeep.append(Variable(column))

print("\nIn this portion, we can specify how to further clean up the dataset.\n")

menuChoice = "none"
while menuChoice != "":
  print(LINE)
  menuChoice = errorChecker("What would you like to do next? ('o' to see options): ", desiredVals = ["o", "v", "s", "d", ""])
  if menuChoice == "o":
    print("Options:")
    print("v: View dataset.")
    print("s: Substitute one or more values for one variable.")
    print("d: Discretize groups. (More options available.)")
    print("ENTER: finished.\n")

  elif menuChoice == "v":
    print(dataframe.head(30))

  elif menuChoice == "s":
    varsToKeep = substituteVals(dataframe, varsToKeep)

  elif menuChoice == "d":
    varsToKeep = discretizeGroups(dataframe, varsToKeep)

  elif menuChoice == "":
    outputFile = errorChecker("Where would you like to write the clinical file? ENTER to use default location: ", desiredVals = "none")
    if outputFile == "":
      outputFile = "default"
    print("Writing clinical file to " + str(outputFile) + ".\n")

with open(oFilePath, 'w+') as myFile:
  myFile.write("Name\tPreferredName\tNewGroups\tSubstitutions\tFileName\n")
  for var in varsToKeep:
    myFile.write("{}\t{}\t{}\t{}\t{}\n".format(var.colName, var.preferredName, var.newGroups, var.substitutions, outputFile))
