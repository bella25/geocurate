from sys import argv
import pandas
import subprocess
import re

LINE = '*'.center(79, '*') + '\n'
 

class Variable():
  """Parent class for class and covariate variables."""
  def __init__(self, colName, naVals, valsToFilter):
    self.colName = colName
    self.naVals = naVals
    self.valsToFilter = valsToFilter
    self.preferredName = "NA"
    self.substitutions = "NA"

#gets user input, and asks again if the user messes up 
def errorChecker(prompt, desiredVals = ["y", "n"]):
  userInput = input(prompt)
  if userInput != "":
    if desiredVals == "integer":
      while type(userInput) == str:
        if userInput.find('-') != -1:
          userInput = userInput.split("-")
          try:
            for element in userInput:
              element = int(element)
            userInput = list(range(int(userInput[0]), int(userInput[1]) + 1))
          except ValueError:
            print("Try again. Please enter two integers separated by '-'.")
            userInput = input(prompt)
        else:
          try:
            userInput = int(userInput)
          except ValueError:
            print("Try again. Please enter an integer.")
            userInput = input(prompt)

    elif desiredVals != "none":
      while userInput not in desiredVals:
        print("Try again. Please enter " + str(desiredVals))
        userInput = input(prompt)
  print("")
    
  return userInput

#prints a head of the column and some stats about range and if the values are all different
def printVarsSummary(dataframe, varName):
  print(LINE)
  if dataframe[varName].dtype == "int64" or dataframe[varName].dtype == "float64":
    print(pandas.DataFrame(dataframe[varName].head(5)))
    print("")
    print("Values range from " + str(min(dataframe[varName])) + " to " + str(max(dataframe[varName])) + ".\n")
  else:
    uniqueVals = pandas.unique(dataframe[varName])
    if len(uniqueVals) == len(dataframe[varName]):
      print('All values for "' + str(varName) + '" are unique. This variable might not be informative.\n')
      print("Here are the first 10 values in the dataset:")
      print(pandas.DataFrame(dataframe[varName].head(10)))
      print("")
    else:
      if len(uniqueVals) <= 1:
        print("All values are the same. This variable might not be informative.\n")
      if len(uniqueVals) > 10:
        print('Here are the first 10 unique values of "' + str(varName) + '":')
        for i in range(10):
          print('\t' + str(uniqueVals[i]))
      else:
        print('The variable "' + str(varName) + '" has these unique values:')
        for value in uniqueVals:
          print('\t' + str(value))
      print("")

#Asks the user which values they would like to substitute
def substituteVals(varToKeep):
  valsToSub = {}
  toSub = errorChecker("Which old value would you like to replace? (or ENTER when finished) ", desiredVals = "none")
  while toSub != "":
    newVal = errorChecker("Which new value would you like to replace it with? ", desiredVals = "none")
    valsToSub[toSub] = newVal
    toSub = errorChecker("Which old value would you like to replace? (or ENTER when finished) ", desiredVals = "none")
  varToKeep.substitutions = valsToSub

#def fixDelimiters() {
#  toSplit = errorChecker("Please indicate the number of the column to be split or ENTER when finished.\nTo indicate a range, type two nums sepd by a hyphen.", desiredVals = "integer")
#  while toSplit != "":
#  splitPoint = errorChecker("Which character would you like to split on? If the character is surrounded by spaces, please include them. ", desiredVals = "none")
#    toSplit = [toSplit] if type(toSplit) == int else toSplit
#    for element in toSplit:
#      print("Element: " + str(element - 1) + " varsList[element]: " + str(varsList[element - 1]))
#      allToSplit[str(str(varsList[element - 1]) + '$')] = splitPoint
#    toSplit = errorChecker("Please indicate the number of the column to be split or ENTER when finished.\nTo indicate a range, type two integers separated by '-': ", desiredVals = "integer")
#}


#def splitVars() {
#  toDivide = errorChecker("Please indicate the number of the column to be broken up or ENTER when finished.\nTo indicate a range, type two integers separated by '-': ", desiredVals = "integer")
#  while toDivide !="":
#    numGroups = errorChecker("How many variables are present in one column? Please enter an integer: ", desiredVals = "integer")
#    colNames = []
#    for i in range(numGroups):
#      colName = errorChecker("Please enter a name for variable " + str(i) + ": ", desiredVals = "none")
#      colNames.append(colName)
#    dividePoint = errorChecker("Which character would you like to split on? If the character is surrounded by spaces, please include them. ", desiredVals = "none")
#    toDivide = [toDivide] if type(toDivide) == int else toDivide
#    for element in toDivide:
#      allToDivide[str(str(varsList[element - 1]) + '$')] = [colNames, str(dividePoint + '%')]
#    toDivide = errorChecker("Please indicate the number of the column to be broken up or ENTER when finished.\nTo indicate a range, type two integers separated by '-': ", desiredVals = "integer")
#}



#download file
filePath = argv[1] 
tempExtractPath = argv[2]
oFilePath = argv[3]

dataframe = pandas.read_table(filePath, index_col = 0)
dataframe.index.name = "SampleID"

#loop through columns for the user to decide what to keep
varsToKeep = [] 
allToSplit = {}
allToDivide = {}
print("\nFirst, we will decide which variables to keep.\n")
for column in dataframe:
  printVarsSummary(dataframe = dataframe, varName = column)
  keepVar = errorChecker(prompt = 'Would you like to use the data in the "' + str(column) + '" column? y/n: ')
  
  if keepVar == "y":
    print('>>>> "' + str(column) + '" will be kept in the output.\n')

    varsToKeep.append(column)

    keyValue = errorChecker("Does this data contain key/value pairs separated by a delimiter (e.g. RELAPSE: 1)? y/n: ")
    if keyValue == "y":
      toSplit = column
      splitPoint = errorChecker("Which character would you like to split on? If the character is surrounded by spaces, please include them. ", desiredVals = "none")
      #print("Element: " + str(element - 1) + " varsList[element]: " + str(varsList[element - 1]))
      allToSplit[toSplit + '$'] = splitPoint


    else:
      multipleVals = errorChecker("Does this column have multiple values in one cell? y/n: ")
      toDivide = []
      if multipleVals == "y":
        toDivide = column
        dividePoint = errorChecker("Which character would you like to split on? If the character is surrounded by spaces, please include them. ", desiredVals = "none")
        #automatically discern how many variables in the column
        values = dataframe.iloc[0][column].split(dividePoint)
        #numGroups = errorChecker("How many variables are present in one column? Please enter an integer: ", desiredVals = "integer")
        colNames = []
        for val in values:
          colName = errorChecker("Please enter a name for variable (" + val + ") or ENTER to exclude: ", desiredVals = "none")
          if colName == "":
            colName = "Exclude_" + val
          colNames.append(colName)
        allToDivide[toDivide + '$'] = [colNames, str(dividePoint + '%')]


    print(LINE)

  else:
    print('>>>>"' + str(column) + '" will be excluded from the output.\n')




#for column in dataframe:
#  if column not in varsToKeep:
#    dataframe.drop(column, axis = 1, inplace = True)

#varsList = list(dataframe)

#dataframe.columns = list(range(1, len(dataframe.columns) + 1))

#decide how to parse the file
#print("\nNow, we will decide which variables need to be split up.\n")
#print(LINE)
#print("\nHere is an example of the data: \n")
#print(dataframe.head(10))
#print("")


print("VarstoKeep: " + str(varsToKeep))
#print("VarsList: " + str(varsList))
print(allToSplit)
print(allToDivide)

print(LINE)
print("Extracting columns...\n")
#exit()

#parse file
command = 'Rscript'
path = 'extractCols.R'
args = [filePath, str(allToSplit), str(allToDivide), tempExtractPath, str(varsToKeep)]
cmd = [command, path] + args
x = subprocess.check_output(cmd, universal_newlines=True)
#print(x)

dataframe = pandas.read_table(tempExtractPath, index_col = 0)
dataframe.index.name = "SampleID"

#loop through columns for the user to decide what to keep
varsToKeep = [] 
print("\nNow, we will decide what to do with extraneous data.\n")
for column in dataframe:
  printVarsSummary(dataframe = dataframe, varName = column)

  naVals = []
  naVal = errorChecker('Please enter any values that should be treated as NA, or ENTER when finished: ', desiredVals = "none")
  while naVal != "":
    naVals.append(naVal)
    naVal = errorChecker('Please enter any values that should be treated as NA, or ENTER when finished: ', desiredVals = "none")

  toFilter = []
  valToFilter = errorChecker('Please enter any values that should be excluded from this variable or ENTER when finished. (To indicate a range, type two numbers separated by a "-"): ', desiredVals = "none")
  while valToFilter != "":
    toFilter.append(valToFilter)
    valToFilter = errorChecker('Please enter any values that should be excluded from this variable or ENTER when finished. (To indicate a range, type two numbers separated by a "-"): ', desiredVals = "none")

  var = Variable(colName = column, naVals = naVals, valsToFilter = toFilter)
  varsToKeep.append(var)
  toSubstitute = errorChecker('Would you like to substitute any of the values in "' + str(column) + '" for new values? y/ENTER: ')
  if toSubstitute == "y":
    substituteVals(var)

  preferredName = errorChecker('Please enter a new name for "' + str(column) + '" or ENTER to keep the current name: ', desiredVals = "none")
  if preferredName == "":
    preferredName = "NA"
  var.preferredName = preferredName

  print(LINE)


print(LINE)
print('That was all the variables. Now writing specification file to "' + str(oFilePath) + '". \n')

#Write a specs file here and parse it using R, then redownload it back into here and use that for the next part
with open(oFilePath, 'w+') as myFile:
  myFile.write("Name\tPreferredName\tNA Vals\tToFilter\tSubstitutions\n")
  for var in varsToKeep:
    myFile.write("{}\t{}\t{}\t{}\t{}\n".format(var.colName, var.preferredName, var.naVals, var.valsToFilter, var.substitutions))

