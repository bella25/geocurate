from sys import argv
import pandas
import re

#gets user input, and asks again if the user messes up 
def errorChecker(prompt, desiredVals = ["y", "n"]):
  userInput = input(prompt)
  if userInput != "":
    if desiredVals == "integer":
      while type(userInput) == str:
        if userInput.find('-') != -1:
          userInput = userInput.split("-")
          try:
            for element in userInput:
              element = int(element)
            userInput = list(range(int(userInput[0]), int(userInput[1]) + 1))
          except ValueError:
            print("Try again. Please enter two integers separated by '-'.")
            userInput = input(prompt)
        else:
          try:
            userInput = int(userInput)
          except ValueError:
            print("Try again. Please enter an integer.")
            userInput = input(prompt)

    elif desiredVals != "none":
      while userInput not in desiredVals:
        print("Try again. Please enter " + str(desiredVals))
        userInput = input(prompt)
  print("")
    
  return userInput

#prints a head of the column and some stats about range and if the values are all different
def printVarsSummary(dataframe, varName):
  print(LINE)
  else:
    uniqueVals = pandas.unique(dataframe[varName])
    if len(uniqueVals) == len(dataframe[varName]):
      print('All values for "' + str(varName) + '" are unique. This variable might not be informative.\n')
      print("Here are the first 10 values in the dataset:")
      print(pandas.DataFrame(dataframe[varName].head(10)))
      print("")
    else:
      if len(uniqueVals) <= 1:
        print("All values are the same. This variable might not be informative.\n")
      if len(uniqueVals) > 10:
        print('Here are the first 10 unique values of "' + str(varName) + '":')
        for i in range(10):
          print('\t' + str(uniqueVals[i]))
      else:
        print('The variable "' + str(varName) + '" has these unique values:')
        for value in uniqueVals:
          print('\t' + str(value))
      print("")

filePath = argv[1] 
dataframe = pandas.read_table(filePath, index_col = 0)
dataframe.index.name = "SampleID"

for varName in dataframe:
  uniqueVals = pandas.unique(dataframe[varName])
  if len(uniqueVals) > 1:
    pattern = r"[A-Za-z]+ [0-9]{1,2},? [0-9]{2,4}"
    for val in uniqueVals:
      if re.search(pattern, val):
        isDate = True
      else:
        isDate = False
        break
    if (isDate):
      print("This GSE has multiple dates in one column and might contain two merged datasets. Here are the first ten values at each date.")
      toSplit = errorChecker("Would you like to unmerge this dataset into multiple datasets? y/n: ")
      if toSplit:
        exit(1)
      
exit(0)
